// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  returnURL: 'http://localhost:4200/pages',
  urlAuthorize: 'https://accounts.spotify.com/authorize',
  urlToken: 'https://accounts.spotify.com/api/token',
  urlPlaylist: 'https://api.spotify.com/v1',
  idClient: 'e263d15696dc4ffaac4198d18384aee3',
  secretClient: '3e2427f67e6447aeaa6aaa9f653debdb',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
